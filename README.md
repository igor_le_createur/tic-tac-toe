# Getting Started with Create React App

# Git commands

### `git clone https://gitlab.com/igor_le_createur/tic-tac-toe.git`

Clone's a repository from GitLab

### `git init`

Initializing a git repository

### `git add . `

Adding all files to repository

### `git commit -m 'NAME_OF_THE_COMMIT'`

Makes a commit with a chosen name

### `git push origin develop`

Pushing commits into develop branch

### `git pull`

Pulls a repository

## Available Scripts

After you downloaded the project, you should use command:

### `npm i`

This command will install node_modules, which are necessary for project to work.

To run a project in development mode use:

### `npm start`

Open http://localhost:3000 to view it in your browser.
The page will reload when you make changes.
You may also see any lint errors in the console.
