import React from 'react'
import { Box } from './Box';
import './Board.css';

export const Board = ({ board, onClick }) => {
    return (
        <div className='board'>
            {board.map((value, idx) => { //idx - give info 'bout wich box was clicled
                return <Box value={value} onClick={() => value === null && onClick(idx)} /> //if value where passing is occupided then passing the onClick func as if it's null and X is there and you won't be albe to update these values
            })}
        </div>

    )
}

export default Board