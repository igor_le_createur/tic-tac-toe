import React from 'react'
import { Link } from 'react-router-dom';


export const Leaderboard = () => {
    const players = [
        { name: 'Igor', games: 10, wins: 10, losses: 0 },
        { name: 'Tom', games: 20, wins: 0, losses: 20 },
        { name: 'Ivan', games: 8, wins: 6, losses: 2 },
    ];
    const topPlayers = players.sort((a, b) => b.wins - a.wins).slice(0, 10);

    return (
        <div>
            <h2>Leaderboard</h2>
            <Link to="/game">Back to the game</Link>
            <table>
                <thead>
                    <tr>
                        <th>Rank</th>
                        <th>Name</th>
                        <th>Games</th>
                        <th>Wins</th>
                        <th>Losses</th>
                    </tr>
                </thead>
                <tbody>
                    {topPlayers.map((player, index) => (
                        <tr key={index}>
                            <td>{index + 1}</td>
                            <td>{player.name}</td>
                            <td>{player.games}</td>
                            <td>{player.wins}</td>
                            <td>{player.losses}</td>
                        </tr>
                    ))}
                </tbody>
            </table>
        </div>
    )
}
