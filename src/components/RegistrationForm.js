import React from 'react';
import { useForm } from 'react-hook-form'

const RegistrationForm = ({ handleRegistration }) => {
    const { register, handleSubmit, formState: { errors } } = useForm();

    const onSubmit = (data) => {
        if (data.username && data.password) {
            performRegistration(data.username, data.password);
        } else {
            alert('Enter username and password');
        }
    };

    const performRegistration = (username, password) => {
        console.log('Registration data:', username, password);
        handleRegistration(username, password);
    };

    return (
        <form onSubmit={handleSubmit(onSubmit)}>
            <h2>Registration</h2>
            <label>
                Name:
                <input type="text" {...register('username', { required: true })} />
                {errors.username && <span>This field is required</span>}
            </label>
            <br />
            <label>
                Password:
                <input type="password" {...register('password', { required: true })} />
                {errors.password && <span>This field is required</span>}
            </label>
            <br />
            <button type="submit">Register</button>
        </form>
    );
};

export default RegistrationForm;
