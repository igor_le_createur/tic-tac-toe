import React, { useState, useEffect, useCallback } from 'react';
import { Board } from './Board';
import { ScoreBoard } from './ScoreBoard';
import { ResetButton } from './ResetButton';

export const Game = () => {
    const winConditons = [
        [0, 1, 2],
        [3, 4, 5],
        [6, 7, 8],
        [0, 3, 6],
        [1, 4, 7],
        [2, 5, 8],
        [0, 4, 8],
        [2, 4, 6]
    ]

    const [scores, setScores] = useState({ xScore: 0, oScore: 0 })
    const [board, setBoard] = useState(Array(9).fill(null));
    const [xPlaying, setXPlaying] = useState(true);
    const [gameOver, setGameOver] = useState(false)
    const [isWaiting, setIsWaiting] = useState(false);


    const handleBoxClick = useCallback((boxIDx) => {
        const updatedBoard = board.map((value, idx) => {
            if (idx === boxIDx) {
                return xPlaying === true ? "X" : "O";
            } else {
                return value;
            }
        })
        const winner = checkWinner(updatedBoard);
        if (winner) {
            if (winner === "O") {
                let { oScore } = scores;
                oScore += 1
                setScores({ ...scores, oScore })
            } else {
                let { xScore } = scores;
                xScore += 1
                setScores({ ...scores, xScore })
            }
        }
        console.log(scores);
        setBoard(updatedBoard);
        setXPlaying(!xPlaying);
    }, [board, xPlaying]);


    const checkWinner = (board) => {
        for (let i = 0; i < winConditons.length; i++) {
            const [x, y, z] = winConditons[i];

            if (board[x] && board[x] === board[y] && board[y] === board[z]) {
                console.log(board[x]);
                setGameOver(true)
                return board[x];
            }
        }
    }


    const resetBoard = () => {
        setGameOver(false);
        setBoard(Array(9).fill(null))
    }


    const makeMove = useCallback(async () => {
        setIsWaiting(true);


        const emptyBoxes = board.reduce((acc, value, idx) => {
            if (value === null) {
                return [...acc, idx];
            }
            return acc;
        }, []);

        const randomIndex = Math.floor(Math.random() * emptyBoxes.length);
        const boxIdx = emptyBoxes[randomIndex];

        await new Promise((resolve) => setTimeout(resolve, 1000));

        setIsWaiting(false);
        handleBoxClick(boxIdx);

    }, [board, handleBoxClick]);


    useEffect(() => {
        if (!xPlaying && !gameOver) {
            makeMove();
        }
    }, [xPlaying, isWaiting, gameOver, makeMove]);
    return (
        <div className="Game">
            <ScoreBoard scores={scores} xPlaying={xPlaying} />
            <Board board={board} onClick={gameOver ? resetBoard : handleBoxClick} />
            <ResetButton resetBoard={resetBoard} />
        </div>
    );
}

