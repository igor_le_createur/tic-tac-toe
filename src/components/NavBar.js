import React from 'react'
import { Link } from 'react-router-dom';


const NavBar = ({ isLoggedIn, username, handleLogout }) => {

    return (
        <nav >

            {isLoggedIn && (

                <div>
                    <div>
                        <Link to="/leaderboard">Leader Board</Link>
                    </div>
                    <div>
                        Logged in as {username}
                    </div>
                    <div>
                        <button onClick={handleLogout}>Logout</button>
                    </div>

                </div>
            )
            }
        </nav >


    );
}



export default NavBar;
