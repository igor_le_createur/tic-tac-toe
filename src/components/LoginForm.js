import React, { useState, useEffect } from 'react';
import { Navigate, Link } from 'react-router-dom';



const LoginForm = ({ handleLogin }) => {
    const [username, setUsername] = useState('');
    const [password, setPassword] = useState('');
    const [loggedIn, setLoggedIn] = useState(false);

    useEffect(() => {
        const storedUsername = localStorage.getItem('username');
        if (storedUsername) {
            setUsername(storedUsername);
        }
    }, []);

    const handleSubmit = (e) => {
        e.preventDefault();

        const storedUsers = localStorage.getItem('registeredUsers');
        const registeredUsers = storedUsers ? JSON.parse(storedUsers) : [];

        const user = registeredUsers.find(
            (user) => user.username === username && user.password === password
        );

        if (user) {
            handleLogin(username);
            setLoggedIn(true);
        } else {
            alert('Invalid username or password');
        }
    };

    if (loggedIn) {
        return <Navigate to="/game" />;
    }

    return (
        <form onSubmit={handleSubmit}>
            <h2>Tic Tac Toe</h2>
            <label>
                username
                <input type="text" value={username} onChange={(e) => setUsername(e.target.value)} />
            </label>
            <br />
            <label>
                password
                <input type="password" value={password} onChange={(e) => setPassword(e.target.value)} />
            </label>
            <br />
            <button type="submit">Log in</button>
            <p>
                Haven't registered yet? <Link to="/registration">Register now</Link>.
            </p>
        </form>
    );
};

export default LoginForm;
