import React, { useState } from 'react';
import { BrowserRouter as Router, Routes, Route, Navigate, Link } from 'react-router-dom';
import './App.css';
import NavBar from './components/NavBar';
import LoginForm from './components/LoginForm';
import RegistrationForm from './components/RegistrationForm';
import { Game } from './components/Game';
import { Leaderboard } from './components/Leaderboard';

function App() {
  const [isLoggedIn, setLoggedIn] = useState(false);
  const [username, setUsername] = useState('');

  const handleRegistration = (username, password) => {

    const existingUsers = localStorage.getItem('registeredUsers');
    const users = existingUsers ? JSON.parse(existingUsers) : [];

    const existingUser = users.find(user => user.username === username);
    if (existingUser) {
      alert('This username is already taken. Please choose a different username.');
      return;
    }

    users.push({ username, password });

    localStorage.setItem('registeredUsers', JSON.stringify(users));

    handleLogin(username);
    setLoggedIn(true);
    setUsername(username);

    const player = { name: username, games: 0, wins: 0, losses: 0 };
    const storedPlayers = localStorage.getItem('players');
    const players = storedPlayers ? JSON.parse(storedPlayers) : [];
    players.push(player);
    localStorage.setItem('players', JSON.stringify(players));
  };


  const handleLogin = (username) => {
    setLoggedIn(isLoggedIn => !isLoggedIn);
    setUsername(username);

  };
  const handleLogout = () => {
    setLoggedIn(false);
    setUsername('');
  };

  return (
    <div className="App">
      <Router>
        <NavBar isLoggedIn={isLoggedIn} username={username} handleLogout={handleLogout} />
        <Routes>
          <Route path="/" element={isLoggedIn ? <Navigate to="/game" /> : <LoginForm handleLogin={handleLogin} />} />
          <Route
            path="/registration"
            element={isLoggedIn ? <Navigate to="/game" /> : <RegistrationForm handleRegistration={handleRegistration} />}
          />
          <Route path="/game" element={<PrivateRoute isLoggedIn={isLoggedIn} />} />
          <Route path="/leaderboard" element={<Leaderboard />} />
        </Routes >
      </Router>
    </div>
  );
}
const PrivateRoute = ({ isLoggedIn }) => {
  return isLoggedIn ? <Game /> : <Navigate to="/" />;
};

export default App;
